const fruits = [
  "apple",
  "pinapple",
  "melon",
  "watermelon",
  "grape",
  "mango",
  "lime",
  "lemon",
];

fruits.forEach((fruit) => console.log(fruit));
